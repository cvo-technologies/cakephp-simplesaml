<?php

namespace CvoTechnologies\SimpleSaml\Test\TestCase\Model\Behavior;

use Cake\Event\Event;
use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use CvoTechnologies\SimpleSaml\Controller\SamlController;
use SimpleSAML_Auth_State;

/**
 * @property int assertActive
 */
class SimpleSamlBehaviorTest extends TestCase
{
    public $fixtures = [
        'plugin.cvo_technologies/simple_saml.users'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->assertActive = assert_options(ASSERT_ACTIVE);
        assert_options(ASSERT_ACTIVE, 0);
    }

    public function tearDown()
    {
        parent::tearDown();

        assert_options(ASSERT_ACTIVE, $this->assertActive);
    }

    public function testHandleLoginEvent()
    {
        $state = [
            'LoginCompletedHandler' => [$this, 'logoutCallback']
        ];
        $stateId = SimpleSAML_Auth_State::saveState($state, 'init');

//        debug($stateId);exit();

        $request = new Request([
            'query' => [
                'state_id' => $stateId
            ],
            'post' => [
                'username' => 'mariano',
                'password' => 'cake',
            ]
        ]);
        $samlController = new SamlController($request);
        $samlController->loadComponent('CvoTechnologies/SimpleSaml.SimpleSaml');
        $samlController->loadComponent('Auth', [
            'authenticate' => [
                'Form'
            ]
        ]);
        $samlController->Auth->setUser($samlController->Auth->identify());
        $samlController->handle();
    }

    public function logoutCallback($state)
    {
        $this->assertEquals([
            'username' => [
                'mariano'
            ]
        ], $state['Attributes']);
    }
}
