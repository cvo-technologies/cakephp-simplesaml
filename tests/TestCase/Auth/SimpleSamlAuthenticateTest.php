<?php

namespace CvoTechnologies\SimpleSaml\Test\TestCase\Auth;

use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\TestSuite\TestCase;
use CvoTechnologies\SimpleSaml\Auth\SimpleSamlAuthenticate;

class SimpleSamlAuthenticateTest extends TestCase
{
    /**
     * @var \CvoTechnologies\SimpleSaml\Auth\SimpleSamlAuthenticate
     */
    public $auth;

    public $fixtures = [
        'plugin.cvo_technologies/simple_saml.users'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Collection = $this->getMockBuilder(ComponentRegistry::class)->getMock();
        $this->auth = new SimpleSamlAuthenticate($this->Collection, [
            'userModel' => 'Users'
        ]);

        $this->response = $this->getMockBuilder(Response::class)->getMock();

        Configure::write('SimpleSaml.saml20-idp-remote.ssocircle', [
            'entityID' => 'https://idp.ssocircle.com',
            'loginField' => 'username'
        ]);
    }

    /**
     * @runInSeparateProcess
     */
    public function testSession()
    {
        $this->markTestIncomplete('Not ready yet');

        \SimpleSAML_Session::getSessionFromRequest()->doLogin('default-sp', [
            'aa' => 'aa',
            'saml:sp:NameID' => [
                'Value' => 'mariano'
            ],
            'Attributes' => [

            ],
            'saml:sp:IdP' => 'https://idp.ssocircle.com'
        ]);

        $request = new Request();

        debug($this->auth->getUser($request));
    }
}
