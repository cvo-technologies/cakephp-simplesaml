<?php

namespace CvoTechnologies\SimpleSaml\Test\TestCase\Middleware;

use Cake\TestSuite\TestCase;
use CvoTechnologies\SimpleSaml\Middleware\SimpleSamlPhpMiddleware;
use SimpleSAML_Error_NotFound;
use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequest;

class SimpleSamlPhpMiddlewareTest extends TestCase
{
    public function testNonSimpleSamlPhpUrl()
    {
        $middleware = new SimpleSamlPhpMiddleware();

        $request = new ServerRequest([], [], '/');

        /* @var \Psr\Http\Message\ResponseInterface $response */
        $response = $middleware($request, new Response, function () {
        });

        $this->assertNull($response);
    }

    public function testAssetLoading()
    {
        $middleware = new SimpleSamlPhpMiddleware();

        $request = new ServerRequest([], [], '/simplesaml/resources/icons/ssplogo-fish-small.png');

        /* @var \Psr\Http\Message\ResponseInterface $response */
        $response = $middleware($request, new Response, function () {
        });

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAssetLoadingWithDefinedMetatype()
    {
        $middleware = new SimpleSamlPhpMiddleware();

        $request = new ServerRequest([], [], '/simplesaml/resources/default.css');

        /* @var \Psr\Http\Message\ResponseInterface $response */
        $response = $middleware($request, new Response, function () {
        });

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('text/css', $response->getHeader('Content-Type')[0]);
    }

    public function testAssetLoadingNotModified()
    {
        $middleware = new SimpleSamlPhpMiddleware();

        $request = new ServerRequest(
            [],
            [],
            '/simplesaml/resources/icons/ssplogo-fish-small.png',
            'GET',
            'Empty',
            [
                'If-Modified-Since' => 'Mon, 1 Aug 2016 11:46:25 GMT'
            ]
        );

        /* @var \Psr\Http\Message\ResponseInterface $response */
        $response = $middleware($request, new Response, function () {
        });

        $this->assertEquals(304, $response->getStatusCode());
    }

    public function testAssetLoadingNotFound()
    {
        $middleware = new SimpleSamlPhpMiddleware();

        $request = new ServerRequest([], [], '/simplesaml/resources/non-existing.png');

        /* @var \Psr\Http\Message\ResponseInterface $response */
        $response = $middleware($request, new Response, function () {
        });

        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testWithoutModuleName()
    {
        $middleware = new SimpleSamlPhpMiddleware([
            'overrideGlobals' => true
        ]);

        $request = new ServerRequest([
            'REQUEST_URI' => 'aa',
            'SCRIPT_FILENAME' => ''
        ], [], '/simplesaml/module.php/');

        $this->expectException(SimpleSAML_Error_NotFound::class);
        $this->expectExceptionMessage('The URL must at least contain a module name followed by a slash.');

        $middleware($request, new Response, function () {
        });
    }

    public function testUnknownModule()
    {
        $middleware = new SimpleSamlPhpMiddleware([
            'overrideGlobals' => true
        ]);

        $request = new ServerRequest([
            'REQUEST_URI' => '/simplesaml/module.php/unknown',
            'SCRIPT_FILENAME' => ''
        ], [], '/simplesaml/module.php/unknown/');

        $this->expectException(SimpleSAML_Error_NotFound::class);
        $this->expectExceptionMessage('The module \\\'unknown\\\' was either not found, or wasn\\\'t enabled.');

        $middleware($request, new Response, function () {
        });
    }
}
