<?php

namespace CvoTechnologies\SimpleSaml\Test\TestCase\Model\Behavior;

use Cake\Event\Event;
use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use CvoTechnologies\SimpleSaml\Controller\SamlController;
use SimpleSAML_Auth_State;

class SimpleSamlComponentTest extends TestCase
{
    public $fixtures = [
        'plugin.cvo_technologies/simple_saml.users'
    ];

    public function testHandleLoginEvent()
    {
        $state = [
            'LoginCompletedHandler' => [$this, 'logoutCallback']
        ];
        $stateId = SimpleSAML_Auth_State::saveState($state, 'init');

//        debug($stateId);exit();

        $request = new Request([
            'query' => [
                'state_id' => $stateId
            ],
            'post' => [
                'username' => 'mariano',
                'password' => 'cake',
            ]
        ]);
        $samlController = new SamlController($request);
        $samlController->loadComponent('CvoTechnologies/SimpleSaml.SimpleSaml');
        $samlController->loadComponent('Auth', [
            'authenticate' => [
                'Form'
            ]
        ]);
        $samlController->Auth->setUser($samlController->Auth->identify());

        $this->assertEquals('Users', $request->session()->read('SimpleSaml.table'));
    }

    public function logoutCallback($state)
    {
        $this->assertEquals([
            'username' => [
                'mariano'
            ]
        ], $state['Attributes']);
    }
}
