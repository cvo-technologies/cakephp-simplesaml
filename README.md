# SimpleSaml plugin

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.txt)
[![Build Status](https://img.shields.io/travis/CVO-Technologies/cakephp-simplesaml/master.svg?style=flat-square)](https://travis-ci.org/CVO-Technologies/cakephp-simplesaml)
[![Coverage Status](https://img.shields.io/codecov/c/github/cvo-technologies/cakephp-simplesaml.svg?style=flat-square)](https://codecov.io/github/cvo-technologies/cakephp-simplesaml)
[![Total Downloads](https://img.shields.io/packagist/dt/cvo-technologies/cakephp-simplesaml.svg?style=flat-square)](https://packagist.org/packages/cvo-technologies/cakephp-simplesaml)
[![Latest Stable Version](https://img.shields.io/packagist/v/cvo-technologies/cakephp-simplesaml.svg?style=flat-square&label=stable)](https://packagist.org/packages/cvo-technologies/cakephp-simplesaml)

## Installation

### Using Composer
```
composer require cvo-technologies/cakephp-simplesaml
```

Ensure `require` is present in `composer.json`:
```json
{
    "require": {
        "cvo-technologies/cakephp-simplesaml": "~1.0"
    }
}
```

### Load the plugin

```php
Plugin::load('CvoTechnologies/SimpleSaml');
```

## Usage
