<?php

use Cake\Event\EventManager;
use Cake\Network\Request;
use CvoTechnologies\SimpleSaml\Database\Type\CertificateType;
use CvoTechnologies\SimpleSaml\Database\Type\SamlMetadataType;
use CvoTechnologies\SimpleSaml\Middleware\SimpleSamlPhpMiddleware;

\Cake\Database\Type::map('saml_metadata', SamlMetadataType::class);
\Cake\Database\Type::map('certificate', CertificateType::class);

putenv('SIMPLESAMLPHP_CONFIG_DIR=' . __DIR__ . DS . 'simplesamlphp');
SimpleSAML_Configuration::setConfigDir(__DIR__ . DS . 'simplesamlphp');

EventManager::instance()->on(
    'Server.buildMiddleware',
    function ($event, \Cake\Http\MiddlewareQueue $middleware) {
        $middleware->insertBefore('\Cake\Routing\Middleware\RoutingMiddleware', new SimpleSamlPhpMiddleware);
    }
);
