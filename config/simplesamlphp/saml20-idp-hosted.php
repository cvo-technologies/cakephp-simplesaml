<?php

foreach ((array)\Cake\Core\Configure::read('SimpleSaml.saml20-idp-hosted') as $hostedIdp) {
    if (isset($hostedIdp['userid']['attribute'])) {
        $hostedIdp['authproc'][] = array(
            'class' => 'core:TargetedID',
            'attributename' => $hostedIdp['userid']['attribute'],
        );
        $hostedIdp['authproc'][] = array(
            'class' => 'saml:PersistentNameID',
            'attribute' => $hostedIdp['userid']['attribute'],
        );
        $hostedIdp['userid.attribute'] = $hostedIdp['userid']['attribute'];

        unset($hostedIdp['userid']['attribute']);
    }
    if (count($hostedIdp['userid'] === 0)) {
        unset($hostedIdp['userid']);
    }
    if (isset($hostedIdp['attributes'])) {
        if (isset($hostedIdp['attributes']['email'])) {
            $hostedIdp['authproc'][] = array(
                'class' => 'saml:AttributeNameID',
                'attribute' => $hostedIdp['attributes']['email'],
                'Format' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
            );
        }

        unset($hostedIdp['attributes']);
    }

    $metadata[$hostedIdp['entityID']] = $hostedIdp;
}
