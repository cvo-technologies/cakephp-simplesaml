<?php

$metadata = [];
foreach (\Cake\Core\Configure::read('SimpleSaml.saml20-idp-remote') as $idp) {
    if ((isset($idp['x509cert'])) && ($idp['x509cert'])) {
        $cert = openssl_x509_read($idp['x509cert']);
        $idp['certFingerprint'] = openssl_x509_fingerprint($cert);
        openssl_x509_free($cert);
    }

    $metadata[$idp['entityID']] = $idp;
}
