<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

Router::scope('/saml', ['authsource' => 'default-sp'], function (RouteBuilder $routeBuilder) {
    $routeBuilder->connect('/login/:idp', [], ['_name' => 'saml_login', 'routeClass' => 'CvoTechnologies/SimpleSaml.Login']);
    $routeBuilder->connect('/logout/:idp', [], ['_name' => 'saml_logout', 'routeClass' => 'CvoTechnologies/SimpleSaml.Logout']);
});

Router::connect('/saml/handle', ['plugin' => 'CvoTechnologies/SimpleSaml', 'controller' => 'Saml', 'action' => 'handle'], ['_name' => 'simplesaml_handle']);
