<?php

namespace CvoTechnologies\SimpleSaml;

use Cake\Network\Request;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use SimpleSAML_Session;
use SimpleSAML_SessionHandler;

class CakeSessionHandler extends SimpleSAML_SessionHandler
{
    /**
     * @var \Cake\Network\Request
     */
    protected $request;

    public static function register(Request $request)
    {
        $class = new ReflectionClass(\SimpleSAML_SessionHandler::class);
        $property = $class->getProperty('sessionHandler');
        $property->setAccessible(true);

        $sessionHandler = new static();
        $sessionHandler->setRequest($request);

//        $property->setValue($sessionHandler);
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create a new session id.
     *
     * @return string The new session id.
     */
    public function newSessionId()
    {
        $this->request->session()->renew();

        return $this->getCookieSessionId();
    }

    /**
     * Retrieve the session ID saved in the session cookie, if there's one.
     *
     * @return string|null The session id saved in the cookie or null if no session cookie was set.
     */
    public function getCookieSessionId()
    {
        return $this->request->session()->id();
    }

    /**
     * Retrieve the session cookie name.
     *
     * @return string The session cookie name.
     */
    public function getSessionCookieName()
    {
        return ini_get('session.name') . '-SimpleSAML';
    }

    /**
     * Save the session.
     *
     * @param \SimpleSAML_Session $session The session object we should save.
     * @return void
     */
    public function saveSession(SimpleSAML_Session $session)
    {
        $this->request->session()->write('SimpleSamlPhp.session', $session);
    }

    /**
     * Load the session.
     *
     * @param string|NULL $sessionId The ID of the session we should load, or null to use the default.
     *
     * @return \SimpleSAML_Session|null The session object, or null if it doesn't exist.
     */
    public function loadSession($sessionId = null)
    {
        assert('is_string($sessionId) || is_null($sessionId)');

        if (!$this->request->session()->check('SimpleSamlPhp.session')) {
            return null;
        }

        return $this->request->session()->read('SimpleSamlPhp.session');
    }

    /**
     * Set a session cookie.
     *
     * @param string $sessionName The name of the session.
     * @param string|null $sessionID The session ID to use. Set to null to delete the cookie.
     * @param array|null $cookieParams Additional parameters to use for the session cookie.
     * @return void
     * @throws \SimpleSAML\Error\CannotSetCookie If we can't set the cookie.
     */
    public function setCookie($sessionName, $sessionID, array $cookieParams = null)
    {
        $this->request->session()->renew();
        \SimpleSAML_Logger::info('Ignoring set cookie');
    }
}
