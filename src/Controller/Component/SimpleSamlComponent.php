<?php

namespace CvoTechnologies\SimpleSaml\Controller\Component;

use Cake\Auth\BaseAuthenticate;
use Cake\Controller\Component;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class SimpleSamlComponent extends Component
{
    /**
     * {@inheritDoc}
     */
    public function implementedEvents()
    {
        return [
            'Auth.afterIdentify' => 'afterIdentify'
        ];
    }

    public function afterIdentify(Event $event, $result, BaseAuthenticate $authenticate)
    {
        $this->request->session()->write('SimpleSaml.table', $authenticate->config('userModel'));
    }

    public function userTable()
    {
        return TableRegistry::get($this->request->session()->read('SimpleSaml.table'));
    }
}
