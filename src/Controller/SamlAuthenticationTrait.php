<?php

namespace CvoTechnologies\SimpleSaml\Controller;

use Cake\Event\Event;
use SimpleSAML_Auth_Source;
use SimpleSAML_Auth_State;

/**
 * @property \Cake\Controller\Component\AuthComponent Auth
 * @property \CvoTechnologies\SimpleSaml\Controller\Component\SimpleSamlComponent SimpleSaml
 */
trait SamlAuthenticationTrait
{
    /**
     * Handle a login started by the SimpleSAMLphp auth source.
     *
     * @return void
     */
    public function handle()
    {
        $this->loadComponent('CvoTechnologies/SimpleSaml.SimpleSaml');

        $query = $this->request->query('state_id');
        $state = SimpleSAML_Auth_State::loadState($query, 'init');

        $userTable = $this->SimpleSaml->userTable();
        $result = $userTable->eventManager()->dispatch(new Event('SimpleSaml.handleLogin', $this, [
            'state' => $state,
            'entity' => $userTable->get($this->Auth->user($userTable->primaryKey()))
        ]));
        $state = $result->data()['state'];

        SimpleSAML_Auth_Source::completeAuth($state);
    }
}
