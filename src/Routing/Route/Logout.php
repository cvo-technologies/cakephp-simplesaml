<?php

namespace CvoTechnologies\SimpleSaml\Routing\Route;

use Cake\Core\Configure;
use Cake\Routing\Exception\RedirectException;
use Cake\Routing\Router;
use Cake\Routing\Route\Route;
use SimpleSAML_Module;

class Logout extends Route
{
    /**
     * {@inheritDoc}
     */
    public function match(array $params, array $context = [])
    {
        if (!isset(Configure::read('SimpleSaml.saml20-idp-remote')[$params['idp']])) {
            return false;
        }

        return parent::match($params, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function parse($url, $method = '')
    {
        $params = parent::parse($url, $method);
        if (!$params) {
            return false;
        }

        if (!isset($params['return'])) {
            $params['return'] = Router::url('/', true);
        }

        $queryParameters = [
            'AuthId' => $params['authsource'],
            'ReturnTo' => Router::url($params['return'], true),
        ];
        if (isset($params['idp'])) {
            $idp = Configure::read('SimpleSaml.saml20-idp-remote')[$params['idp']];

            $queryParameters['saml:idp'] = $idp['entityID'];
        }

        throw new RedirectException(SimpleSAML_Module::getModuleURL('core/as_logout.php', $queryParameters), 303);
    }
}
