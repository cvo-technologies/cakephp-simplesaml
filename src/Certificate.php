<?php

namespace CvoTechnologies\SimpleSaml;

/**
 * Represents a certificate and its information.
 */
class Certificate
{
    protected $certificateData;

    /**
     * Certificate constructor.
     *
     * @param string $certificateData The base64 encoded data of the certificate without the headers.
     */
    public function __construct($certificateData)
    {
        $this->certificateData = $certificateData;
    }

    /**
     * Return a formatted certificate with headers.
     *
     * @return string
     */
    public function format()
    {
        return '-----BEGIN CERTIFICATE-----' . PHP_EOL . $this->certificateData . PHP_EOL . '-----END CERTIFICATE-----' . PHP_EOL;
    }

    /**
     * Return the certificate data without headers.
     *
     * @return string
     */
    public function certificateData()
    {
        return $this->certificateData;
    }

    /**
     * Return a formatted certificate with headers.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->format();
    }
}
