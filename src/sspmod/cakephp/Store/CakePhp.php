<?php

class sspmod_cakephp_Store_CakePhp extends SimpleSAML_Store
{
    /**
     * @var \Cake\Network\Request
     */
    public static $request;

    /**
     * Retrieve a value from the data store.
     *
     * @param string $type The data type.
     * @param string $key The key.
     *
     * @return mixed|null The value.
     */
    public function get($type, $key)
    {
        return static::$request->session()->read('SimpleSaml.' . $key);
    }

    /**
     * Save a value to the data store.
     *
     * @param string $type The data type.
     * @param string $key The key.
     * @param mixed $value The value.
     * @param int|null $expire The expiration time (unix timestamp), or null if it never expires.
     * @return void
     */
    public function set($type, $key, $value, $expire = null)
    {
        static::$request->session()->write('SimpleSaml.' . $key, $value);
    }

    /**
     * Delete a value from the data store.
     *
     * @param string $type The data type.
     * @param string $key The key.
     * @return void
     */
    public function delete($type, $key)
    {
        static::$request->session()->delete('SimpleSaml.' . $key);
    }
}
