<?php

use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Routing\DispatcherFactory;
use Cake\Routing\Router;

class sspmod_cakephp_Auth_Source_CakePhp extends SimpleSAML_Auth_Source
{
    /**
     * The string used to identify our states.
     */
    const STAGE_INIT = 'cakephp:init';

    /**
     * Process a request.
     *
     * If an authentication source returns from this function, it is assumed to have
     * authenticated the user, and should have set elements in $state with the attributes
     * of the user.
     *
     * If the authentication process requires additional steps which make it impossible to
     * complete before returning from this function, the authentication source should
     * save the state, and at a later stage, load the state, update it with the authentication
     * information about the user, and call completeAuth with the state array.
     *
     * @param array $state Information about the current authentication.
     * @return void
     */
    public function authenticate(&$state)
    {
        assert('is_array($state)');

        $stateId = SimpleSAML_Auth_State::saveState($state, 'init');

        //SAML2_HTTPRedirect::
        \SimpleSAML\Utils\HTTP::redirectTrustedURL(Router::url([
            '_name' => 'simplesaml_handle',
            'state_id' => $stateId,
        ], true));
    }
}
