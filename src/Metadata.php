<?php

namespace CvoTechnologies\SimpleSaml;

use Cake\Core\InstanceConfigTrait;
use Cake\Utility\Xml;

class Metadata
{
    use InstanceConfigTrait;

    protected $xml;

    protected $_defaultConfig = [];

    public function __construct($config, $xml)
    {
        $this->config($config);
        $this->xml = $xml;
    }

    public function __toString()
    {
        return $this->xml;
    }

    public static function fromXml($xmlString)
    {
        $xml = Xml::toArray(Xml::build($xmlString));

        $config = [
            'entityId' => $xml['EntityDescriptor']['@entityID']
        ];

        $idpServices = ['SingleSignOnService', 'SingleLogoutService', 'ArtifactResolutionService', 'ManageNameIDService', 'NameIDMappingService'];
        $spServices = ['AssertionConsumerService', 'SingleLogoutService'];

        //region IdP config normalization
        if (isset($xml['EntityDescriptor']['IDPSSODescriptor'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor'] = $xml['EntityDescriptor']['IDPSSODescriptor'];
        }
        foreach ($idpServices as $idpService) {
            if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor'][$idpService])) {
                $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:' . $idpService] = $xml['EntityDescriptor']['md:IDPSSODescriptor'][$idpService];
            }
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['KeyDescriptor'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:KeyDescriptor'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['KeyDescriptor'];
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['NameIDFormat'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:NameIDFormat'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['NameIDFormat'];
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:NameIDFormat'])) {
            $config['NameIDFormat'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:NameIDFormat'];
        }
        //endregion
        //region SP config normalization
//        if (isset($xml['EntityDescriptor']['IDPSSODescriptor'])) {
//            $xml['EntityDescriptor']['md:IDPSSODescriptor'] = $xml['EntityDescriptor']['IDPSSODescriptor'];
//        }
//        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleSignOnService'])) {
//            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleSignOnService'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleSignOnService'];
//        }
//        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleLogoutService'])) {
//            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleLogoutService'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleLogoutService'];
//        }
//        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['KeyDescriptor'])) {
//            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:KeyDescriptor'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['KeyDescriptor'];
//        }
//        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['NameIDFormat'])) {
//            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:NameIDFormat'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['NameIDFormat'];
//        }
        //endregion

        foreach ($idpServices as $idpService) {
            if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:' . $idpService])) {
                $config[$idpService] = static::convertServiceDefinition(
                    $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:' . $idpService]
                );
            }
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:KeyDescriptor'])) {
            $mdKeyDescriptor = $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:KeyDescriptor'];
            if (isset($mdKeyDescriptor['ds:KeyInfo'])) {
                $mdKeyDescriptor = [$mdKeyDescriptor];
            }
            foreach ($mdKeyDescriptor as $key) {
                if ($key['@use'] !== 'signing') {
                    continue;
                }

                $config['certData'] = str_replace([
                    "\n", "\r", ' '
                ], '', $key['ds:KeyInfo']['ds:X509Data']['ds:X509Certificate']);
            }
        }

        if (isset($xml['EntityDescriptor']['md:SPSSODescriptor']['md:NameIDFormat'])) {
            $config['NameIDFormat'] = $xml['EntityDescriptor']['md:SPSSODescriptor']['md:NameIDFormat'];
        }
        foreach ($spServices as $spService) {
            if (isset($xml['EntityDescriptor']['md:SPSSODescriptor']['md:' . $spService])) {
                $config[$spService] = static::convertServiceDefinition(
                    $xml['EntityDescriptor']['md:SPSSODescriptor']['md:' . $spService]
                );
            }
        }

        return new static($config, $xmlString);
    }

    protected static function convertServiceDefinition($service)
    {
        if (isset($service['@Binding'])) {
            $service = [
                $service
            ];
        }

        $serviceConfigs = [];
        foreach ($service as $serviceDefinition) {
            $serviceConfig = [];
            foreach ($serviceDefinition as $attribute => $value) {
                $attribute = substr($attribute, 1);
                if ($attribute === 'index') {
                    $value = (int)$value;
                }

                $serviceConfig[$attribute] = $value;
            }

            $serviceConfigs[] = $serviceConfig;
        }

        return $serviceConfigs;
    }
}
