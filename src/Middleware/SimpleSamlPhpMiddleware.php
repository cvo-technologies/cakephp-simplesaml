<?php

namespace CvoTechnologies\SimpleSaml\Middleware;

use Cake\Core\InstanceConfigTrait;
use Cake\Filesystem\File;
use Cake\Http\RequestTransformer;
use Cake\Network\Request;
use CvoTechnologies\SimpleSaml\CakeSessionHandler;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use SimpleSAML_Error_BadRequest;
use SimpleSAML_Error_NotFound;
use SimpleSAML_Logger;
use SimpleSAML_Module;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class SimpleSamlPhpMiddleware
{
    use InstanceConfigTrait;

    /**
     * The amount of time to cache the asset.
     *
     * @var string
     */
    protected $cacheTime = '+1 day';

    /**
     * A extension to content type mapping for plain text types.
     *
     * Because finfo doesn't give useful information for plain text types,
     * we have to handle that here.
     *
     * @var array
     */
    protected $typeMap = [
        'css' => 'text/css',
        'json' => 'application/json',
        'js' => 'application/javascript',
        'ico' => 'image/x-icon',
        'eot' => 'application/vnd.ms-fontobject',
        'svg' => 'image/svg+xml',
        'html' => 'text/html',
        'rss' => 'application/rss+xml',
        'xml' => 'application/xml',
    ];

    protected $_defaultConfig = [
        'overrideGlobals' => false
    ];

    public function __construct(array $config = [])
    {
        $this->config($config);
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next)
    {
        $cakeRequest = RequestTransformer::toCake($request);
        \sspmod_cakephp_Store_CakePhp::$request = $cakeRequest;

        $path = $request->getUri()->getPath();
        $baseURL = \SimpleSAML_Configuration::getConfig()->getBaseURL();
        if (strpos($path, $baseURL) !== 1) {
            return $next($request, $response);
        }

        if ($this->config('overrideGlobals')) {
            $this->overrideGlobals($request);
        }

        if (strpos($path, $baseURL . 'resources/') === 1) {
            $resourcePath = \SimpleSAML_Configuration::getConfig()->getBaseDir() . 'www/resources/';

            $file = new File($resourcePath . substr($path, strlen($baseURL . 'resources/') + 1));
            if (!$file->exists()) {
                return $this->sendNotFound($response);
            }

            $modifiedTime = $file->lastChange();
            if ($this->isNotModified($request, $file)) {
                $headers = $response->getHeaders();
                $headers['Last-Modified'] = date(DATE_RFC850, $modifiedTime);

                return new Response('php://memory', 304, $headers);
            }

            return $this->deliverAsset($request, $response, $file);
        }
        $wwwDir = \SimpleSAML_Configuration::getConfig()->getBaseDir() . 'www/';
        $file = $wwwDir . substr($path, strlen($baseURL . '/'));
        if ((strpos($path, $baseURL . 'module.php/') !== 1) && (file_exists($file))) {
            chdir(dirname($file));

            require $file;

            exit;
        }
        if (strpos($path, $baseURL . 'module.php/') !== 1) {
            return $next($request, $response);
        }

        CakeSessionHandler::register(Request::createFromGlobals());

        // index pages - file names to attempt when accessing directories
        $indexFiles = ['index.php', 'index.html', 'index.htm', 'index.txt'];

        $url = substr($path, strlen($baseURL . 'module.php/'));
        assert('substr($url, 0, 1) === "/"');

        $modEnd = strpos($url, '/', 1);
        if ($modEnd === false) {
            // the path must always be on the form /module/
            throw new SimpleSAML_Error_NotFound('The URL must at least contain a module name followed by a slash.');
        }

        $module = substr($url, 1, $modEnd - 1);
        $url = substr($url, $modEnd + 1);
        if ($url === false) {
            $url = '';
        }

        if (!SimpleSAML_Module::isModuleEnabled($module)) {
            throw new SimpleSAML_Error_NotFound('The module \'' . $module . '\' was either not found, or wasn\'t enabled.');
        }

        $moduleDir = SimpleSAML_Module::getModuleDir($module) . '/www/';

        // check for '.php/' in the path, the presence of which indicates that another php-script should handle the request
        for ($phpPos = strpos($url, '.php/'); $phpPos !== false; $phpPos = strpos($url, '.php/', $phpPos + 1)) {
            $newURL = substr($url, 0, $phpPos + 4);
            $param = substr($url, $phpPos + 4);

            if (is_file($moduleDir . $newURL)) {
                /* $newPath points to a normal file. Point execution to that file, and
                 * save the remainder of the path in PATH_INFO.
                 */
                $url = $newURL;
                $_SERVER['PATH_INFO'] = $param;
                break;
            }
        }

        $path = $moduleDir . $url;

        if ($path[strlen($path) - 1] === '/') {
            // path ends with a slash - directory reference. Attempt to find index file in directory
            foreach ($indexFiles as $if) {
                if (file_exists($path . $if)) {
                    $path .= $if;
                    break;
                }
            }
        }

        if (is_dir($path)) {
            return $next($request, $response);
        }

        if (!file_exists($path)) {
            // file not found
            SimpleSAML_Logger::info('Could not find file \'' . $path . '\'.');
            throw new SimpleSAML_Error_NotFound('The URL wasn\'t found in the module.');
        }

        if (!preg_match('#\.php$#D', $path)) {
            return $next($request, $response);
        }

        ob_start();
        require $path;

        exit();
    }

    protected function overrideGlobals(ServerRequestInterface $serverRequest)
    {
        $_SERVER = $serverRequest->getServerParams();
        $_GET = $serverRequest->getQueryParams();
    }

    /**
     * Check the not modified header.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request to check.
     * @param \Cake\Filesystem\File $file The file object to compare.
     * @return bool
     */
    protected function isNotModified($request, $file)
    {
        $modifiedSince = $request->getHeaderLine('If-Modified-Since');
        if (!$modifiedSince) {
            return false;
        }

        return strtotime($modifiedSince) === $file->lastChange();
    }

    /**
     * Sends an asset file to the client
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request object to use.
     * @param \Psr\Http\Message\ResponseInterface $response The response object to use.
     * @param \Cake\Filesystem\File $file The file wrapper for the file.
     * @return \Psr\Http\Message\ResponseInterface The response with the file & headers.
     */
    protected function deliverAsset(ServerRequestInterface $request, ResponseInterface $response, $file)
    {
        $contentType = $this->getType($file);
        $modified = $file->lastChange();
        $expire = strtotime($this->cacheTime);
        $maxAge = $expire - time();

        $stream = new Stream(fopen($file->path, 'rb'));

        return $response->withBody($stream)
            ->withHeader('Content-Type', $contentType)
            ->withHeader('Cache-Control', 'public,max-age=' . $maxAge)
            ->withHeader('Date', gmdate('D, j M Y G:i:s \G\M\T', time()))
            ->withHeader('Last-Modified', gmdate('D, j M Y G:i:s \G\M\T', $modified))
            ->withHeader('Expires', gmdate('D, j M Y G:i:s \G\M\T', $expire));
    }

    /**
     * Return the type from a File object
     *
     * @param \Cake\Filesystem\File $file The file from which you get the type
     * @return string
     */
    protected function getType($file)
    {
        $extension = $file->ext();
        if (isset($this->typeMap[$extension])) {
            return $this->typeMap[$extension];
        }

        return $file->mime() ?: 'application/octet-stream';
    }

    protected function sendNotFound(ResponseInterface $response)
    {
        return $response->withStatus(404);
    }
}
