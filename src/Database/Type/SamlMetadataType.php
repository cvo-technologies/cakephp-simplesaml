<?php

namespace CvoTechnologies\SimpleSaml\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use CvoTechnologies\SimpleSaml\Metadata;

class SamlMetadataType extends Type
{
    public function toPHP($value, Driver $driver)
    {
        if ($value === null) {
            return null;
        }

        return Metadata::fromXml($value);
    }

    public function marshal($value)
    {
        if ($value instanceof Metadata) {
            return $value;
        }
        if (!trim($value)) {
            return null;
        }

        return Metadata::fromXml($value);
    }
}
