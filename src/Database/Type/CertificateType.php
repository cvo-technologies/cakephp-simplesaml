<?php

namespace CvoTechnologies\SimpleSaml\Database\Type;

use Cake\Database\Driver;
use Cake\Database\Type;
use CvoTechnologies\SimpleSaml\Certificate;

class CertificateType extends Type
{
    public function toPHP($value, Driver $driver)
    {
        if (empty($value)) {
            return null;
        }

        return new Certificate($value);
    }

    public function toDatabase($value, Driver $driver)
    {
        if ($value instanceof Certificate) {
            return $value->certificateData();
        }
        if ((strstr($value, '-----BEGIN CERTIFICATE-----')) && (strstr($value, '-----END CERTIFICATE-----'))) {
            $value = str_replace([
                '-----BEGIN CERTIFICATE-----',
                '-----END CERTIFICATE-----',
                PHP_EOL,
                ' '
            ], '', $value);
        }

        return $value;
    }

    public function marshal($value)
    {
        if ($value instanceof Certificate) {
            return $value;
        }
        if ((strstr($value, '-----BEGIN CERTIFICATE-----')) && (strstr($value, '-----END CERTIFICATE-----'))) {
            $value = str_replace([
                '-----BEGIN CERTIFICATE-----',
                '-----END CERTIFICATE-----',
                PHP_EOL,
                ' '
            ], '', $value);
        }

        return new Certificate($value);
    }
}
