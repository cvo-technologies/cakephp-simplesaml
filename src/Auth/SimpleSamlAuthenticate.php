<?php

namespace CvoTechnologies\SimpleSaml\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\Network\Request;
use Cake\Network\Response;
use CvoTechnologies\SimpleSaml\CakeSessionHandler;
use CvoTechnologies\SimpleSaml\Saml;
use OneLogin_Saml2_Auth;
use sspmod_cakephp_Store_CakePhp;

class SimpleSamlAuthenticate extends BaseAuthenticate
{
    protected $_defaultConfig = [
        'userModel' => 'Users',
        'scope' => [],
        'finder' => 'all',
        'contain' => null,
        'passwordHasher' => 'Default'
    ];

    /**
     * {@inheritDoc}
     */
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
            'Auth.logout' => 'onLogout'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function authenticate(Request $request, Response $response)
    {
        return $this->getUser($request);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser(Request $request)
    {
        sspmod_cakephp_Store_CakePhp::$request = $request;
        CakeSessionHandler::register($request);

        $authState = \SimpleSAML_Session::getSessionFromRequest()->getAuthState('default-sp');
        if (!$authState) {
            return false;
        }

        $idp = Saml::getIdpFromIssuer($authState['saml:sp:IdP']);

        $idpConfig = Saml::buildIdpConfig($idp);

        $this->_config['fields']['username'] = $idpConfig['loginField'];

        return $this->_findSamlUser($this->_getAttributes($authState, $idpConfig['attributes']), $idpConfig);
    }

    /**
     * Find the user provided by the SAML session.
     *
     * @param array $attributes The attributes provided by the session.
     * @param array $idpConfig The config used for the IdP.
     * @return bool
     */
    protected function _findSamlUser($attributes, array $idpConfig)
    {
        $result = $this->_query($attributes['login'])->first();
        if (empty($result)) {
            return false;
        }

        EventManager::instance()->dispatch(new Event('Saml.userFound', $this, [
            'config' => $idpConfig,
            'user' => $result,
            'attributes' => $attributes
        ]));

        return $result->toArray();
    }

    /**
     * Get the attributes mapped to local field names.
     *
     * @param array $authState The SAML state information.
     * @param array $attributeMapping The mapping between IdP attributes and local fields.
     * @return array
     */
    protected function _getAttributes($authState, $attributeMapping)
    {
        $loginField = $attributeMapping['login'];
        $attributes = [
            'login' => ($loginField === 'nameId') ? $authState['saml:sp:NameID']['Value'] : $authState['Attributes'][$loginField][0]
        ];

        foreach ($attributeMapping as $localField => $samlAttribute) {
            if ($localField === 'login') {
                continue;
            }
            if (!isset($authState['Attributes'][$samlAttribute])) {
                $attributes[$localField] = null;

                continue;
            }

            $attributes[$localField] = $authState['Attributes'][$samlAttribute];
        }

        foreach (array_diff(array_keys($authState['Attributes']), array_values($attributeMapping)) as $attribute) {
            Log::notice(__d(
                'cvo_technologies/saml_login',
                'Attribute "{0}" from IdP "{1}" has not been mapped',
                $attribute,
                $authState['saml:sp:IdP']
            ));
        }

        return $attributes;
    }

    /**
     * {@inheritDoc}
     */
    public function onLogout(Event $event)
    {
        $session = \SimpleSAML_Session::getSessionFromRequest();

        $session->doLogout('default-sp');
        $session->doLogout('cakephp');
        $session->save();
    }
}
