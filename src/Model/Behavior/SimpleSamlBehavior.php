<?php

namespace CvoTechnologies\SimpleSaml\Model\Behavior;

use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;

/**
 * Handles logins when using the plugin as IdP.
 */
class SimpleSamlBehavior extends Behavior
{
    protected $_defaultConfig = [
        'attributes' => [
        ],
    ];

    /**
     * {@inheritDoc}
     */
    public function implementedEvents()
    {
        return parent::implementedEvents() + [
            'SimpleSaml.handleLogin' => 'handleLogin'
        ];
    }

    /**
     * Handle a login from the SimpleSAMLphp auth module.
     *
     * This method will load the information from the provided entity to the Attributes list of the session.
     *
     * @param Event $event The event dispatched with state information.
     * @param array $state The state information passed from the auth module.
     * @param Entity $entity The entity to read argument data from.
     * @return void
     */
    public function handleLogin(Event $event, $state, Entity $entity)
    {
        foreach ($this->config('attributes') as $field => $attribute) {
            $event->data['state']['Attributes'][$attribute][] = $entity->get($field);
        }
    }
}
