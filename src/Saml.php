<?php

namespace CvoTechnologies\SimpleSaml;

use Cake\Core\Configure;
use Cake\Network\Request;
use Cake\Routing\Router;
use Cake\Utility\Hash;
use Cake\Utility\Xml;
use OneLogin_Saml2_Auth;
use SimpleXMLElement;

class Saml
{
    public static function buildAuth($idp)
    {
        return new OneLogin_Saml2_Auth(static::buildConfig($idp));
    }

    public static function buildConfig($idp)
    {
        $config = [
            'strict' => true,
            'debug' => true,
            'security' => Configure::read('Saml.security'),
            'contactPerson' => Configure::read('Saml.contactPerson'),
            'organization' => Configure::read('Saml.organization'),
            'sp' => static::buildSpConfig(),
        ];
        if ($idp) {
            $config['idp'] = static::buildIdpConfig($idp);
        }

        return $config;
    }

    public static function buildSettings($idp = null)
    {
        return new \OneLogin_Saml2_Settings(static::buildConfig($idp), !$idp);
    }

    public static function buildIdpConfig($idp)
    {
        return Hash::merge([
            'attributes' => [
                'login' => 'nameId'
            ]
        ], Configure::read('SimpleSaml.saml20-idp-remote.' . $idp));
    }

    public static function buildSpConfig()
    {
        $config = Configure::read('Saml.sp');
        $config['assertionConsumerService']['url'] = Router::url(['_name' => 'saml_callback'], true);
        $config['singleLogoutService']['url'] = Router::url(['_name' => 'saml_logout_callback'], true);

        return $config;
    }

    public static function getIdpFromIssuer($issuer)
    {
        return key(collection(Configure::read('SimpleSaml.saml20-idp-remote'))->match([
            'entityID' => $issuer
        ])->toArray());
    }

    public static function getResponse(Request $request)
    {
        return new \OneLogin_Saml2_Response(static::buildSettings(), $request->data('SAMLResponse'));
    }

    public static function getLogoutResponse(Request $request)
    {
        return new \OneLogin_Saml2_LogoutResponse(static::buildSettings(), $request->query('SAMLResponse'));
    }

    public static function convertMetdataToConfig(SimpleXMLElement $xml)
    {
        $xml = Xml::toArray($xml);

        $config = [
            'entityId' => $xml['EntityDescriptor']['@entityID']
        ];
        if (isset($xml['EntityDescriptor']['IDPSSODescriptor'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor'] = $xml['EntityDescriptor']['IDPSSODescriptor'];
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleSignOnService'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleSignOnService'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleSignOnService'];
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleLogoutService'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleLogoutService'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['SingleLogoutService'];
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['KeyDescriptor'])) {
            $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:KeyDescriptor'] = $xml['EntityDescriptor']['md:IDPSSODescriptor']['KeyDescriptor'];
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleSignOnService'])) {
            foreach ($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleSignOnService'] as $key) {
                if ($key['@Binding'] !== 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST') {
                    continue;
                }

                $config['singleSignOnService']['url'] = $key['@Location'];
            }
        }
        if (isset($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleLogoutService'])) {
            foreach ($xml['EntityDescriptor']['md:IDPSSODescriptor']['md:SingleLogoutService'] as $key) {
                if ($key['@Binding'] !== 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect') {
                    continue;
                }

                $config['singleLogoutService']['url'] = $key['@Location'];
            }
        }

        $mdKeyDescriptor = $xml['EntityDescriptor']['md:IDPSSODescriptor']['md:KeyDescriptor'];
        if (isset($mdKeyDescriptor['ds:KeyInfo'])) {
            $mdKeyDescriptor = [$mdKeyDescriptor];
        }
        foreach ($mdKeyDescriptor as $key) {
            if ($key['@use'] !== 'signing') {
                continue;
            }

            $config['x509cert'] = $key['ds:KeyInfo']['ds:X509Data']['ds:X509Certificate'];
        }

        return $config;
    }
}
